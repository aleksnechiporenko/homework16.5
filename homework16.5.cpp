﻿
#include <iostream>
#include <ctime>

int main()
{
    setlocale(LC_ALL, "Russian");

    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int iday = timeinfo.tm_mday;


    const int size = 6;

    int array[size][size];

    int sumElements = 0;

    for (int i = 0; i < size; ++i )
    {
        for (int j = 0; j < size; ++j)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";

            if (iday % size == i)
            {
                sumElements += array[i][j];
            }
        }

        std::cout << "\n";
    }

    std::cout << "\nТекущее календарное число - " << iday << "\n\n";
    std::cout << "Остаток деления текущего числа календаря на N (size) - iday % size = " << iday % size << "\n\n";
    std::cout << "Cумма элементов в строке массива, индекс которой равен " << iday % size << " - " << sumElements << "\n";
    
}


